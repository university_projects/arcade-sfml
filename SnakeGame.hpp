#ifndef SNAKE_GAME_HPP
#define SNAKE_GAME_HPP

#include "Game.hpp"
#include "Snake.hpp"

class SnakeGame : public Game
{
public:
	SnakeGame();
	void update(float deltaTime) override;

private:
	void generateFood();
	void gameOver();

	float stepTime;
	float currentTime;
	Snake snake;
	Led food;
};

#endif // SNAKE_GAME_HPP
