#include "Tetris.hpp"
#include "Gamepad.hpp"
#include "Matrix.hpp"

extern Gamepad gamepad;
extern Matrix matrix;

////////////////////////////////////////////////
Tetris::Tetris()
{
	piece.generate();
	writeInMatrix();
}

////////////////////////////////////////////////
void Tetris::writeInMatrix()
{
	for (int i = 0; i < 4; i++)
		matrix [(int) piece.leds [i].x] [(int) piece.leds [i].y] = true;
}

////////////////////////////////////////////////
void Tetris::clearInMatrix()
{
	for (int i = 0; i < 4; i++)
		matrix [(int) piece.leds [i].x] [(int) piece.leds [i].y] = false;
}

////////////////////////////////////////////////
bool Tetris::checkAvailability(Piece piece)
{
	for (int i = 0; i < 4; i++)
	{
		if (piece.leds [i].x == -1 || piece.leds [i].x == COLUMNS ||
			piece.leds [i].y == -1 ||
			matrix [(int) piece.leds [i].x] [(int) piece.leds [i].y])
			return false;
	}
	return true;
}
////////////////////////////////////////////////
bool Tetris::rotate()
{
	clearInMatrix();

	clone = piece;
	clone.rotate();

	if (checkAvailability(clone))
		piece.rotate();

	writeInMatrix();
}

////////////////////////////////////////////////
void Tetris::update(float deltaTime)
{
	if (gamepad.wasKeyReleased(Gamepad::A))
		rotate();

	if (gamepad.wasKeyReleased(Gamepad::Down))
		move(Direction::Down);

	if (gamepad.wasKeyReleased(Gamepad::Left))
		move(Direction::Left);

	if (gamepad.wasKeyReleased(Gamepad::Right))
		move(Direction::Right);

}

///////////////////////////////////////////////
void Tetris::move(Direction direction)
{
	clearInMatrix();
	piece.move(direction);

	if (!checkAvailability(piece))
		piece.move(Direction(-(int)direction));

	writeInMatrix();
}
