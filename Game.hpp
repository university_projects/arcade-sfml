#ifndef GAME_HPP
#define GAME_HPP

#define COLUMNS 8
#define ROWS	16

enum class GameType
{
	Tetris,
	Snake
};

class Game
{
public:
	Game() : running(true) {}
	virtual ~Game() {}
	virtual void update(float deltaTime) = 0;

	bool running;
};

#endif // GAME_HPP
