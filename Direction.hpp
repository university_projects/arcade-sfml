#ifndef DIRECTION_HPP
#define DIRECTION_HPP

enum Direction
{
	Up = 1,
	Down = -1,
	Left = -2,
	Right = 2
};

#endif // DIRECTION_HPP
