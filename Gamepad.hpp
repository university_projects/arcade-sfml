#ifndef GAMEPAD_HPP
#define GAMEPAD_HPP

#include <SFML/Window/Keyboard.hpp>

class Gamepad
{
public:
	enum Key
	{
		Up	 	= sf::Keyboard::Up,
		Down	= sf::Keyboard::Down,
		Left	= sf::Keyboard::Left,
		Right	= sf::Keyboard::Right,
		A		= sf::Keyboard::A
	};

	enum KeyState
	{
		Normal,
		Pressed,
		Released
	};

	Gamepad();
	bool isKeyPressed(Key key) const;
	bool wasKeyReleased(Key key) const;
	void update();

private:
	KeyState upState;
	KeyState downState;
	KeyState rightState;
	KeyState leftState;
	KeyState aState;
};

#endif // GAMEPAD_HPP
