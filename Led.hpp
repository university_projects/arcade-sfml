#ifndef LED_HPP
#define LED_HPP

typedef char UChar;

struct Led
{
	Led(UChar x = 0, UChar y = 0) : x(x), y(y) {}
	UChar x;
	UChar y;
};

#endif // LED_HPP
