#include "Matrix.hpp"
#include "Assets.h"

////////////////////////////////////////////////
Matrix::Matrix()
{
	/// Init the matrix
	for (int row = 0; row < ROWS; row++)
		for (int column = 0; column < COLUMNS; column++)
			matrix [column] [row] = false;

	font.loadFromFile(ASSETS"kingthings.ttf");
	text.setFont(font);
	text.setCharacterSize(30);
}

////////////////////////////////////////////////
Matrix::~Matrix()
{
}

////////////////////////////////////////////////
void Matrix::setup()
{
	window.create(sf::VideoMode(400, 700), "arcade a la cham");
	window.setFramerateLimit(30);
}

////////////////////////////////////////////////
bool Matrix::isOpen() const
{
	return window.isOpen();
}

////////////////////////////////////////////////
void Matrix::close()
{
	window.close();
}

////////////////////////////////////////////////
void Matrix::clear(bool clearLogic)
{
	if (clearLogic)
		for (int row = 0; row < ROWS; row++)
			for (int column = 0; column < COLUMNS; column++)
				matrix [column] [row] = false;

	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();
	}

	window.clear();
}

////////////////////////////////////////////////
void Matrix::print()
{
	sf::String string;

	for (int row = 15; row >= 0; row--)
	{
		for (int column = 0; column < COLUMNS; column++)
			string += (matrix [column] [row]) ? "0 " : "X ";

		string += "\n";
	}

	text.setString(string);
	window.draw(text);
	window.display();
}

////////////////////////////////////////////////
bool * Matrix::operator[](unsigned int column)
{
	return matrix [column];
}
