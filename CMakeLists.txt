cmake_minimum_required(VERSION 2.8)
project(arcade)

configure_file(Assets.h.cmake ${CMAKE_SOURCE_DIR}/Assets.h)

set(SRC main.cpp
		Gamepad.cpp
		Matrix.cpp
		Menu.cpp
		Piece.cpp
		Random.cpp
		Snake.cpp
		SnakeGame.cpp
		Tetris.cpp)
add_executable(arcade ${SRC})
target_link_libraries(arcade -lsfml-graphics -lsfml-window -lsfml-system)
