#ifndef TETRIS_HPP
#define TETRIS_HPP

#include "Piece.hpp"
#include "Game.hpp"

class Tetris : public Game
{
public:
	Tetris();
	bool rotate();
	void update(float deltaTime) override;
	void move (Direction direction);

private:
	void clearInMatrix ();
	void writeInMatrix();
	bool checkAvailability(Piece piece);
	Piece piece;
	Piece clone;
};

#endif // TETRIS_HPP
