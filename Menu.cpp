#include "Menu.hpp"
#include "Gamepad.hpp"
#include "Matrix.hpp"

extern Gamepad gamepad;
extern Matrix matrix;

////////////////////////////////////////////////
Menu::Menu() : selectedGame(GameType::Tetris)
{
    writeGameCover();
}

////////////////////////////////////////////////
void Menu::update(float deltaTime)
{
    if (gamepad.wasKeyReleased(Gamepad::Left) || gamepad.wasKeyReleased(Gamepad::Right))
    {
        if (gamepad.wasKeyReleased(Gamepad::Left))
        {
            switch (selectedGame)
            {
                case GameType::Tetris: selectedGame = GameType::Snake; break;
                case GameType::Snake: selectedGame = GameType::Tetris; break;
            }
        }
        else
        {
            switch (selectedGame)
            {
                case GameType::Tetris: selectedGame = GameType::Snake; break;
                case GameType::Snake: selectedGame = GameType::Tetris; break;
            }
        }

        writeGameCover();
    }

    if (gamepad.wasKeyReleased(Gamepad::A))
    {
        matrix.clear(true);
        running = false;
    }
}

////////////////////////////////////////////////
GameType Menu::getSelectedGame()
{
    return selectedGame;
}

////////////////////////////////////////////////
void Menu::writeGameCover()
{
    matrix.clear(true);
    switch (selectedGame)
    {
        case GameType::Tetris:

            matrix [0] [0] = true;
            matrix [0] [1] = true;
            matrix [1] [0] = true;
            matrix [3] [0] = true;

            for (int column = 4; column < COLUMNS; column++)
                for (int row = 0; row < 2; row++)
                    matrix [column] [row] = true;

            matrix [1] [13] = true;
            matrix [2] [13] = true;
            matrix [3] [13] = true;
            matrix [2] [12] = true;
        break;

        case GameType::Snake:
            matrix [1] [5] = true;
            matrix [1] [8] = true;
            matrix [1] [9] = true;
            matrix [1] [10] = true;
            matrix [2] [10] = true;
            matrix [3] [10] = true;
            matrix [3] [9] = true;
            matrix [4] [9] = true;
    }

    matrix.print();
}
