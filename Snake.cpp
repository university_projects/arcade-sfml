#include "Snake.hpp"

////////////////////////////////////////////////
Snake::Snake() : size(2), currentDirection(Up), newDirection(Up), needNewPiece(false)
{
	head = new SnakePiece(4, 7);
	tail = new SnakePiece(4, 6);

	head->next = tail;
	tail->prev = head;
}

////////////////////////////////////////////////
Snake::~Snake()
{
	SnakePiece *nextPiece = head;
	SnakePiece *pieceToDelete = nullptr;

	while (nextPiece)
	{
		pieceToDelete = nextPiece;
		nextPiece = pieceToDelete->next;
		delete pieceToDelete;
	}
}

////////////////////////////////////////////////
void Snake::setDirection(Direction direction)
{
	newDirection = direction;
}

////////////////////////////////////////////////
void Snake::move()
{
	/// Move all the pieces except the head
	SnakePiece *piece = tail;

	SnakePiece *newPiece;
	if (needNewPiece)
		newPiece = new SnakePiece(tail->x, tail->y);

	while (piece->prev)
	{
		piece->x = piece->prev->x;
		piece->y = piece->prev->y;
		piece = piece->prev;
	}

	if (newDirection == - currentDirection)
		newDirection = currentDirection;

	/// Move the head
	switch (newDirection)
	{
		case Up: head->y++; break;
		case Down: head->y--; break;
		case Right: head->x++; break;
		case Left: head->x--;
	}

	currentDirection = newDirection;

	if (needNewPiece)
	{
		tail->next = newPiece;
		newPiece->prev = tail;
		tail = newPiece;
		size++;
		needNewPiece = false;
	}
}

////////////////////////////////////////////////
void Snake::eat()
{
	needNewPiece = true;
}

////////////////////////////////////////////////
int Snake::getSize() const
{
	return size;
}

////////////////////////////////////////////////
const SnakePiece * Snake::operator[](int index) const
{
	if (index >= size)
		return nullptr;

	if (index == size - 1)
		return tail;

	int position = 0;
	SnakePiece *piece = head;

	while (position < index)
	{
		piece = piece->next;
		position++;
	}

	return piece;
}
