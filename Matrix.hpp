#ifndef MATRIX_DRIVER_HPP
#define MATRIX_DRIVER_HPP

#include <SFML/Graphics.hpp>

#define COLUMNS 8
#define ROWS	16


class Matrix
{
public:
	Matrix();
	~Matrix();

	bool isOpen() const;
	void close();
	void setup();
	void clear(bool clearLogic = false);
	void print();

	bool * operator[](unsigned int column);

private:
	bool matrix [8] [16];

	sf::RenderWindow window;
	sf::Font font;
	sf::Text text;
};

#endif // MATRIX_DRIVER_HPP
