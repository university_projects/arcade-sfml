#ifndef MENU_HPP
#define MENU_HPP

#include "Game.hpp"

class Menu : public Game
{
public:
    Menu();
    void update(float deltaTime) override;
    GameType getSelectedGame();

private:
    void writeGameCover();
    GameType selectedGame;
};

#endif // MENU_HPP
