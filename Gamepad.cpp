#include "Gamepad.hpp"

////////////////////////////////////////////////
Gamepad::Gamepad()
{
	upState = downState = rightState = leftState = aState = Normal;
}

////////////////////////////////////////////////
bool Gamepad::isKeyPressed(Key key) const
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Key(key));
}

////////////////////////////////////////////////
bool Gamepad::wasKeyReleased(Key key) const
{
	switch (key)
	{
		case Up: return upState == Released;
		case Down: return downState == Released;
		case Right: return rightState == Released;
		case Left: return leftState == Released;
		case A: return aState == Released;
	}
}

////////////////////////////////////////////////
void Gamepad::update()
{
	Key keys [5] = {Up, Down, Right, Left, A};
	KeyState *keysStates [5] = {&upState, &downState, &rightState, &leftState, &aState};

	for (int i = 0; i < 5; i++)
	{
		KeyState *keyState = keysStates [i];
		Key key = keys [i];

		switch (*keyState)
		{
			case Normal:
				if (isKeyPressed(key))
					(*keyState) = Pressed;
			break;

			case Pressed:
				if (! isKeyPressed(key))
					(*keyState) = Released;
			break;

			case Released:
				(*keyState) = Normal;
		}
	}
}
