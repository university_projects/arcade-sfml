#include "Game.hpp"
#include "Menu.hpp"
#include "Tetris.hpp"
#include "SnakeGame.hpp"
#include "Matrix.hpp"
#include "Gamepad.hpp"

#include <stdlib.h>
#include <time.h>
#include <SFML/System/Clock.hpp>

Gamepad gamepad;
Matrix matrix;

int main()
{
	srand(time(nullptr));
	matrix.setup();
	Menu *menu = new Menu;

	while (menu->running)
	{
		gamepad.update();
		menu->update(0);
	}

	matrix.clear(true);
	Game *game;
	switch (menu->getSelectedGame())
	{
		case GameType::Tetris: delete menu; game = new Tetris; break;
		case GameType::Snake: delete menu; game = new SnakeGame;
	}

	sf::Clock clock;
	while (matrix.isOpen())
	{
		gamepad.update();
		game->update(clock.restart().asMilliseconds());
		matrix.clear();
		matrix.print();
	}

	delete game;
	return 0;
}
